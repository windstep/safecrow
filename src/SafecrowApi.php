<?php

namespace Windstep\Safecrow;

use Windstep\Safecrow\Traits\SendsSafecrowRequestsTrait;

class SafecrowApi
{
    use SendsSafecrowRequestsTrait;

    public function getUsers()
    {
        return self::request("GET", '/users');
    }

    public function createUser(array $attributes)
    {
        return self::request('POST', '/users', $attributes);
    }

    public function findUser(int $id)
    {
        return self::request('GET', '/users/' . $id);
    }

    public function findUserByEmail($email){
        return self::request('GET', '/users?email='.$email);
    }

    public function editUser(int $id, array $attributes)
    {
        return self::request('POST', '/users/' . $id, $attributes);
    }

    public function calculateFee(array $attributes)
    {
        return self::request('POST', '/calculate', $attributes);
    }

    public function createOrder(array $attributes)
    {
        return self::request('POST', '/orders', $attributes);
    }

    public function findOrder(int $id){
        return self::request('GET', '/orders/'. $id);
    }

    public function getOrders(){
        return self::request('GET', '/orders');
    }

    public function viewOrdersByUser(int $id)
    {
        return self::request('GET', '/users/' . $id . '/orders');
    }

    public function annulOrder(int $id, array $attributes)
    {
        return self::request('POST', '/orders/' . $id . '/annul');
    }

    public function payOrder(int $id, array $attributes)
    {
        return self::request('POST', '/orders/' . $id . '/pay');
    }

    public function attachCardToUser(int $id, array $attributes)
    {
        return self::request('POST', '/users/'. $id . '/cards');
    }

    public function viewAttachedCards(int $id){
        return self::request('GET', '/users/'. $id . '/cards');
    }

    public function attachCardToOrder(int $user_id, int $order_id, array $attributes){
        return self::request('GET', '/users/' . $user_id . '/orders/' . $order_id);
    }

    public function cancelOrder(int $id, array $attributes){
        return self::request('GET', '/orders/'.$id.'/cancel', $attributes);
    }

    public function closeOrder(int $id){
        return self::request('GET', '/orders/'. $id . '/close');
    }

    public function escalate(int $id, array $attributes){
        return self::request('POST', '/orders/'. $id . '/escalate', $attributes);
    }

    public function addAttachment(int $id, array $attributes){
        return self::request('POST', '/orders/'. $id . '/attachments');
    }

    public function getAttachments(int $id){
        return self::request('GET', '/orders/'. $id . '/attachments');
    }
}