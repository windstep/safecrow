<?php

namespace Windstep\Safecrow\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Windstep\Safecrow\Exceptions\SafecrowAuthorizationException;

trait SendsSafecrowRequestsTrait
{
    /**
     * Внутренний метод, позволяющий сгенерировать рабочую подпись для запросов Safecrow.
     * Описание работы можно найти в документации.
     *
     * @param string $endpoint Представляет собой строку вида '/users?email=email@mail.mail'
     * @param string $method Представляет собой строку вида 'POST' 'GET' и других
     * @return string HMAC подпись для запроса
     */
    public static function generateHmacSign(string $endpoint, string $method)
    {
        $data = config('safecrow.api_key') . $method . config('safecrow.api_prefix') . $endpoint;
        return hash_hmac('SHA256', $data, config('safecrow.api_secret'));
    }

    /**
     * Метод позволяет отправить запрос на сервера SafeCrow
     * Возвращает ответ от этих серверов.
     *
     * @param string $method
     * @param string $endpoint
     * @param array $body
     * @return \Psr\Http\Message\StreamInterface
     * @throws SafecrowAuthorizationException
     */
    public static function request(string $method, string $endpoint, array $body = null)
    {
        $client = new Client(['base_uri' => config('safecrow.api_url')]);
        $hmac = self::generateHmacSign($endpoint, $method);
        try{
            $response = $client->request($method, config('safecrow.api_prefix') . $endpoint,
                [
                    "auth" => [config('safecrow.api_key'), $hmac],
                    RequestOptions::JSON => $body,
                    "defaults" => ["verify" => false],
                    "headers" => [
                        "Content-Type" => "application/json"
                    ]
                ]);
            return json_decode($response->getBody(), true);
        }catch(GuzzleException $e){
            if($e->getCode() == 404)
                throw new ModelNotFoundException();
            if($e->getCode() == 401)
                throw new SafecrowAuthorizationException($hmac, $endpoint);
            report($e);
        }

    }
}