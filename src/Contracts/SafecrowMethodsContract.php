<?php

namespace Windstep\Safecrow\Contracts;

interface SafecrowMethodsContract
{
    /**
     * Генерирует HMAC пароль для SafeCrow
     *
     * @param string $endpoint Конечная точка запроса ('/users?email=qwerty@qwerty.qw')
     * @param string $qweryType Тип запроса ('GET', 'POST', 'DELETE", 'PUT')
     * @return string HMAC подпись тела запроса ключом
     */
    public function generateHmacSign(string $endpoint, string $qweryType="GET") : string;

    /**
     * Отправляет запрос на сервера SafeCrow и возвращает ответ оттуда
     *
     * @return mixed
     */
    public function request(string $method, string $endpoint, array $body = null) : \Psr\Http\Message\StreamInterface;

    /**
     * Пытаемся зарегистрировать пользователя
     *
     * @return mixed
     */
    public function registerUser();

    public function showUsers();

    public function showUser();

    public function updateUser();

    public function calculateFee();

    public function createOrder();

    /**
     * Создает заказ с оплатой за доставку
     *
     * @return mixed
     */
    public function createAcquiringOrder();

    public function viewOrders();

    /**
     * Отменяет заказ до его оплаты
     *
     * @return mixed
     */
    public function annulOrder();

    public function pay();

    public function attachCard();

    public function viewAttachedCards();

    public function attachCardToOrder();

    /**
     * Отменяет заказ после оплаты
     *
     * @return mixed
     */
    public function cancelOrder();

    /**
     * Закрывает сделку, как успешную
     *
     * @return mixed
     */
    public function closeOrder();

    /**
     * Претензия к сделке
     *
     * @return mixed
     */
    public function complain();

    /**
     * Прикрепляет вложение к сделке
     *
     * @return mixed
     */
    public function attach();

    /**
     * Позволяет просмотреть вложения конкретной сделки
     *
     * @return mixed
     */
    public function viewAttaches();

    /**
     * Позволяет настроить URL для коллбэка
     *
     * @return mixed
     */
    public function setup();

    public function reauthorizePay();
}