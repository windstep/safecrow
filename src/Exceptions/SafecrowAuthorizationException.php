<?php

namespace Windstep\Safecrow\Exceptions;

use Throwable;

class SafecrowAuthorizationException extends \Exception
{
    private $api_key;
    private $api_hashsign;
    private $api_secret;

    public function __construct(string $api_hashsign = '', string $endpoint = '', string $message = "", int $code = 0, Throwable $previous = null)
    {
        if (!$message)
            $message = "Error while trying to reach Safecrow API. API_KEY: " . config('safecrow.api_key') . ' API_SECRET: ' . config('safecrow.api_secret') . ' API_HASSIGN: ' . $api_hashsign . ' API_ENDPOINT ' . $endpoint;
        parent::__construct($message, $code = 0, $previous = null);
    }
}