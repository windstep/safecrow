<?php

return [
    'api_key' => env('SAFECROW_API_KEY'),
    'api_secret' => env('SAFECROW_API_SECRET'),

    /**
     * Префикс для обращения к корректному API.
     * Здесь и далее перед url ставится /, после не ставится.
     */
    'api_prefix' => '/api/v3',
    'api_dev_url' => 'https://dev.safecrow.ru',
    'api_url' => 'https://safecrow.ru'
];